<?php

/**
 * Copyright (C) 2020 Tencent Cloud.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace App\Console\Commands\Upgrades;

use App\Common\PermissionKey;
use App\Models\Group;
use App\Models\Permission;
use Discuz\Base\DzqLog;
use Discuz\Console\AbstractCommand;

class AddVideoAndAttachmentPermissionCommand extends AbstractCommand
{
    protected $signature = 'upgrade:addVideoAndAttachmentPermission';
    protected $description = '添加所有用户查看视频、查看附件、下载附件权限';

    protected $permissions = [
        PermissionKey::THREAD_VIEW_VIDEO            => [10,7],  // 查看视频
        PermissionKey::THREAD_VIEW_ATTACHMENT       => [10,7],  // 查看附件
        PermissionKey::THREAD_DOWNLOAD_ATTACHMENT   => [10,7],  // 下载附件
    ];

    public function handle()
    {
        $this->info('脚本执行 [开始]');
        $this->info('');

        try {
            $groups = Group::query()->where('id', '>=', Group::GUEST_ID)->get(['id'])->pluck('id')->toArray();
            foreach ($this->permissions as $key => &$val) {
                $val = $groups;
            }

            $data = collect($this->permissions)->map(function ($value, $key) {
                return collect($value)->map(function ($value) use ($key) {
                    return [
                        'group_id' => $value,
                        'permission' => $key
                    ];
                });
            })->reduce(function ($value, $item) {
                return $item->merge($value);
            });

            Permission::query()->insertOrIgnore($data->toArray());
        } catch (\Exception $e) {
            DzqLog::error('add_video_and_attachment_permission_command_error', [], $e->getMessage());
            $this->info('脚本执行 [异常]');
        }

        $this->info('');
        $this->info('脚本执行 [完成]');
    }
}
